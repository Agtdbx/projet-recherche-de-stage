const arrowLeft = $(".arrow-left")
const arrowRight = $(".arrow-right")
const slider = $(".article-slider")
const articleWidth = $(".article").height()
const len = $(".article").length - 2.8
const valAdd = articleWidth;

$(document).ready(function(){
    val = 0;
    arrowLeft.click(function(){
        if(val < 0){
            val+=valAdd
            slider.transition({x: val+'px'})
        }
    })

    arrowRight.click(function(){
        console.log(val);
        if(val > -len*articleWidth){
            val-=valAdd
            slider.transition({x: val+'px'})
        }
    })
})

