function onSignIn(googleUser) { //fonction donnée en partie par l'API google permettant de se connecter avec GOOGLE.COM
    var profile = googleUser.getBasicProfile(); // on creer un profile qu'on rempli de donnée, l'API s'occupe de les attribuers
    console.log("ID: " + profile.getId());
    console.log('Full Name: ' + profile.getName());
    console.log('Given Name: ' + profile.getGivenName());
    console.log('Family Name: ' + profile.getFamilyName());
    console.log("Image URL: " + profile.getImageUrl());
    console.log("Email: " + profile.getEmail());

        // un token est générée, manipulable plus tard en backend.
    var id_token = googleUser.getAuthResponse().id_token;
    console.log("ID Token: " + id_token);
      }
