from selenium import webdriver 
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support import ui
from time import sleep

def goToNextPage(driver):
    buttons = driver.find_elements_by_class_name("nav_tab_result")
    nb_buttons = len(buttons)
    if nb_buttons == 0:
        return False
    if nb_buttons == 1:
        button_text = buttons[0].text
        if "Suivant" in button_text:
            button_link = buttons[0].get_attribute("href")
            driver.get(button_link)
            return True
        else:
            return False
    else:
        driver.get(buttons[1].get_attribute("href"))
        return True

def getLinksPage(driver):
    links = set()
    liste_entreprises = driver.find_elements_by_css_selector("td > a")
    for elem in liste_entreprises:
                link = elem.get_attribute("href")
                links.add(link)
    return links

def getAllLinks(driver):
    links_list = getLinksPage(driver)
    while goToNextPage(driver):
        links_list.update(getLinksPage(driver))

    return links_list

def webElemsAsText(WebElementList):
    """ Transforme une liste de webElement en liste du texte qu'il contient. """
    res = list()
    for webElem in WebElementList:
        res.append(webElem.text)
    return res

def ficheEntAsDict(nom_ent, libels, infos):
    """
    Transforme une fiche entreprise en distionnaire
    param: - libels: liste des libels de la fiche
           - infos: liste des infos de la fiche
    """
    fiche_ent = dict()
    fiche_ent["Nom entreprise"] = nom_ent.text
    for i in range(len(libels)):
        libel_clean = libels[i].rstrip(" :")
        fiche_ent[libel_clean] = infos[i]
    return fiche_ent

def getAllFiche(link_list, driver):

    list_fiche_ent = list()

    for link in link_list:
        driver.get(link)
        nom_ent = driver.find_element_by_xpath("//div[@class='col-sm-12 fiche_nom']")

        libel_info = driver.find_elements_by_xpath("//div[@class='row']//span[@class='libel_fiche']")
        info_ent = driver.find_elements_by_xpath("//div[@class='row']//span[@class='cont_fiche']")

        libel_info_not_wanted = driver.find_elements_by_xpath("//div[@class='row'][@id='info_general' or @id='info_internat' or @id='info_chiffres']//span[@class='libel_fiche']")
        info_ent_not_wanted = driver.find_elements_by_xpath("//div[@class='row'][@id='info_general' or @id='info_internat' or @id='info_chiffres']//span[@class='cont_fiche']")

        libel_info_clean = [elem for elem in libel_info if elem not in libel_info_not_wanted]
        info_ent_clean = [elem for elem in info_ent if elem not in info_ent_not_wanted]

        libel_info_clean_text = webElemsAsText(libel_info_clean)
        info_ent_clean_text = webElemsAsText(info_ent_clean)

        fiche_ent = ficheEntAsDict(nom_ent, libel_info_clean_text, info_ent_clean_text)

        list_fiche_ent.append(fiche_ent)

    return list_fiche_ent

def scrapping():
    driver = webdriver.Firefox()
    driver.get("https://www.fichentreprise.com/")
    search_bar = driver.find_element_by_name("rech_enseigne")

    search_bar.send_keys("informatique")
    search_bar.send_keys(Keys.ENTER)
    sleep(10)

    links_list = getAllLinks(driver)
    fiches = getAllFiche(links_list, driver)

    driver.quit()
    return fiches

