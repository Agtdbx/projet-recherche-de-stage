import hashlib
from os import system, urandom

from .app import app, db
from .models import *
import click
import csv
from .utils import get_element
import re
from .codeAPE import recupInfo
from .scraping_fichentreprise import scrapping

def has_numbers(inputString):
    return bool(re.search(r'\d', inputString))

@app.cli.command()
@click.argument('filename')
def loaddb(filename):
    """Creations des tables et de la Base de donnée"""
    db.drop_all() #supression des anciennes table pour eviter tout overwrite
    db.create_all()

    print("Enregistrement de la database")

    article = Article(titre="exemple de titre", fileName="article1.html", image="http://plan-de-financement.fr/wp-content/uploads/2011/10/exemple-plan-de-financement.jpg", auteur_id=1)
    db.session.add(article)
    db.session.commit()

    web = Preference(name="Web")
    logiciel = Preference(name="Logiciel")
    systeme = Preference(name="Systeme")
    reseau = Preference(name="Réseau")
    db.session.add(web)
    db.session.add(logiciel)
    db.session.add(systeme)
    db.session.add(reseau)
    db.session.commit()


    adr = Adresse(voie="16 Rue D'Issoudin", code=45100, ville="Orléans")
    db.session.add(adr)
    db.session.commit()

    password = "admin" #création d'un admin vierge pour les tests
    s = urandom(32)
    key = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), s, 100000)
    admin = User(prenom="Admin", nom="User", email="admin.admin@gmail.com", salt=s, key=key, admin=True,
                 adresse=adr)
    db.session.add(admin)
    db.session.commit()

    infoAPE = recupInfo()
    print("SECTEUR")
    for key in infoAPE.keys():
        value = infoAPE[key]
        print("APE :", key, "\tnom :", value)
        secteur = Secteur(nom=value, ape=key)
        db.session.add(secteur)
        db.session.commit()

    print("ENTREPRISE")

    def clearStr(string):
            rep = ""
            rmv = False
            for c in string:
                if c == "(":
                    rmv = True
                elif c == ")":
                    rmv = False
                elif not rmv:
                    rep += c
            return rep

    fiches = scrapping()

    for resultat in fiches:
        nom = resultat.get("Nom entreprise", "Nom inconnu")
        tel = resultat.get("Téléphone", "Téléphone inconnu")
        effectif = clearStr(resultat.get("Effectif", "Effectif inconnu"))

        ape = resultat.get("Code APE", "APE inconnu")
        if ape != "APE inconnu":
            ape = ape[0:5]

        adresse = resultat.get("Adresse", "Adresse inconnu")

        print(nom, adresse, effectif, tel, ape, infoAPE.get(ape, "Secteur inconnu"))

        adr = Adresse(voie=adresse, ville="")
        db.session.add(adr)
        db.session.commit()

        APE = Secteur(ape=ape, nom=infoAPE.get(ape, "Secteur inconnu"))
        db.session.add(APE)
        db.session.commit()

        e = Entreprise(nom=nom, blacklist=False, adresse=adr, taille=effectif, ape=ape, tel=tel, activite=infoAPE.get(ape, "Secteur inconnu"))

        db.session.add(e)
        db.session.commit()

    # with open('data.csv', newline='') as csvfile: #entrée des comptes existants
    #     reader = csv.DictReader(csvfile)
    #     for row in reader:
    #         print("Nom : ", row["Nom de l'entreprise"], ", Adresse : ", row['Adresse'], ", Code postal : ",
    #               row["Code postal"], ", Ville : ",
    #               row["Ville"])
    #         e = Entreprise(nom=row["Nom de l'entreprise"], blacklist=False)
    #         if(has_numbers(row['Adresse'])):
    #             numVoie = re.search('[0-9]+', row['Adresse']).group()
    #             nomVoie = row['Adresse'].split(numVoie)[1]
    #             adr = Adresse(voie=numVoie + " " + nomVoie, code=row["Code postal"], ville=row["Ville"])
    #             db.session.add(adr)
    #             db.session.commit()

    #             e.adresse = adr
    #             db.session.add(e)
    #             db.session.commit()
    #         else :
    #             adr = Adresse(voie=row['Adresse'], code=row["Code postal"], ville=row["Ville"])
    #             db.session.add(adr)
    #             db.session.commit()

    #             e.adresse = adr
    #             db.session.add(e)
    #             db.session.commit()
