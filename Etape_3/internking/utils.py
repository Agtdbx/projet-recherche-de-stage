import hashlib
import datetime
import os
import sqlite3
import sys
import threading
from typing import Type

from .app import db
from .models import User, Token, Preference, Adresse, Entreprise, Article

import googlemaps 

def modify_element(model, id, dict):
    res = db.session.get(model, id)
    for key, value in dict.items():
        if(isinstance(value, list)):
            setattr(res, key, value[0])
        else: 
            setattr(res, key, value)
    db.session.commit()


def get_element(model, id):
    return model.query.filter_by(id=id).first()


def all_of_elements(model: Type[db.Model]):
    return model.query.all()


def create_user(name, surname, nomv, code, city, mail, password, prefs):
    s = os.urandom(32)
    v = User.query.filter_by(email=mail).first()
    if not isinstance(v, User):
        key = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), s, 100000)
        a = Adresse(voie=nomv, code=code, ville=city)
        db.session.add(a)
        db.session.commit()
        preferences = []
        for pId in prefs :
            preferences.append(get_element(Preference, pId))
        if len(preferences)>0:
            u = User(email=mail, prenom=name, nom=surname, admin=False, salt=s, key=key, adresse=a, preferences=preferences)
        else :
            u = User(email=mail, prenom=name, nom=surname, admin=False, salt=s, key=key, adresse=a)
        db.session.add(u)
        u = User.query.filter_by(email=mail).first()
        db.session.commit()
        return True
    return False

def modify_user(id, modifications):
    v = User.query.filter_by(id=id).first()
    addr = Adresse.query.filter_by(id=v.adresse_id).first()
    if v!=None:
        for key, value in modifications.items():
            if "prefs" in key :
                preferences = []
                for pId in value :
                    preferences.append(get_element(Preference, pId))
                setattr(v, "preferences", preferences)
            else :
                if "adresse." in key :
                    newK = key.split("adresse.")[1]
                    setattr(addr, newK, value[0])
                else :
                    if(isinstance(value, list)):
                        setattr(v, key, value[0])
                    else: 
                        setattr(v, key, value)
        db.session.commit()
        return True
    return False

def get_pref(name):
    pref = Preference.query.filter_by(name=name).first()
    if not isinstance(pref, Preference):
        newP = Preference(name=name)
        return newP
    return pref


def create_token(id):
    u = User.query.filter_by(id=id).first()
    v = Token.query.filter_by(user_id=u.id).first()
    if isinstance(v, Token):
        db.session.delete(v)
    token = os.urandom(32)
    t = Token(user=u, user_id=u.id, date=datetime.date.today(), token=str(token))
    db.session.add(t)
    db.session.commit()
    return str(token)


def verify_login(id, given_pass):
    user = User.query.filter_by(id=id).first()
    if isinstance(user, User):
        new_key = hashlib.pbkdf2_hmac('sha256', given_pass.encode('utf-8'), user.salt, 100000)
        if new_key == user.key:
            return True
    return False


def verify_token(given_token):
    t = Token.query.filter_by(token=str(given_token)).first()
    if isinstance(t, Token) and (t.date - datetime.date.today()).total_seconds() < 3600 * 1:
        return True
    return False

# =============================
# Recherche
# =============================
      
def getDistance(origin, destination):
    gmaps = googlemaps.Client(key='AIzaSyBV7V0ZAfw9xU2ThKgtjLVbl7dcFxORSw8')  
    val = gmaps.distance_matrix(origin, destination)['rows'][0]['elements'][0]
    res = val.get("distance", dict())
    dist = res.get("text", "Incon")
    if ("Incon" in dist):
        return 20037.5
    else:
        noKm = ""
        for i in range(len(dist)-2):
            noKm += dist[i]
        num = float(noKm)
        return num


class myThread (threading.Thread):
    def __init__(self, threadID, adresseEnt, adresseUser):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.adresseEnt = adresseEnt
        self.adresseUser = adresseUser
        self.res = []
    
    def run(self):
        for info in self.adresseEnt:
            id = info[0]
            adrEnt = info[1]
            dist = getDistance(adrEnt, self.adresseUser)
            self.res.append((id, dist))

    def getRes(self):
        return self.res


def search(name, place, activitySector, distance, size, favories, numPage, user):
    ents = Entreprise.query.filter(Entreprise.nom.like("%"+name+"%"))
    fav = get_favoris(user.id)
    entreprises = []
    adresse_ent = []
    for e in ents:
        e.dist = 5
        adresse_ent.append((e.id, e.adresse.voie + ", " + e.adresse.ville))
        if favories == "true":
            if e in fav:
                if (place == ""):
                    if (size == "0"):
                        entreprises.append(e)
                    elif (size == "1" and int(e.taille) < 10):
                        entreprises.append(e)
                    elif (size == "10" and (int(e.taille) >= 10 and int(e.taille) < 100)):
                        entreprises.append(e)
                    elif (size == "100" and int(e.taille) > 100):
                        entreprises.append(e)
                    elif (e.taille == "Effectif inconnu"):
                        entreprises.append(e)
                else:
                    if (place in e.adresse.voie + ", " + e.adresse.ville):
                        if (size == "0"):
                            entreprises.append(e)
                        elif (size == "1" and int(e.taille) < 10):
                            entreprises.append(e)
                        elif (size == "10" and (int(e.taille) >= 10 and int(e.taille) < 100)):
                            entreprises.append(e)
                        elif (size == "100" and int(e.taille) > 100):
                            entreprises.append(e)
                        elif (e.taille == "Effectif inconnu"):
                            entreprises.append(e)
        else:
            if (place == ""):
                if (size == "0"):
                    entreprises.append(e)
                elif (size == "1" and int(e.taille) < 10):
                    entreprises.append(e)
                elif (size == "10" and (int(e.taille)>= 10 and int(e.taille) < 100)):
                    entreprises.append(e)
                elif (size == "100" and int(e.taille) > 100):
                    entreprises.append(e)
                elif (e.taille == "Effectif inconnu"):
                    entreprises.append(e)
            else:
                if (place in e.adresse.voie + ", " + e.adresse.ville):
                    if (size == "0"):
                        entreprises.append(e)
                    elif (size == "1" and int(e.taille) < 10):
                        entreprises.append(e)
                    elif (size == "10" and (int(e.taille) >= 10 and int(e.taille) < 100)):
                        entreprises.append(e)
                    elif (size == "100" and int(e.taille) > 100):
                        entreprises.append(e)
                    elif (e.taille == "Effectif inconnu"):
                        entreprises.append(e)
    
    nbVal = len(adresse_ent)//10

    listThread = []
    if (nbVal>=1):
        for i in range(nbVal):
            if i == nbVal-1:
                listThread.append(myThread(i, adresse_ent[nbVal*i:len(adresse_ent)], user.adresse.voie + ", " + user.adresse.ville))
            else:
                listThread.append(myThread(i, adresse_ent[i*nbVal:(i+1)*nbVal], user.adresse.voie + ", " + user.adresse.ville))
    else:
        listThread.append(myThread(1, adresse_ent, user.adresse.voie + ", " + user.adresse.ville))

    for th in listThread:
        th.start()

    distEnt = [] 
    for th in listThread:
        th.join()
        distEnt.extend(th.getRes())

    print("D :", distEnt)
    dist = dict()
    for info in distEnt:
        dist[info[0]] = info[1]
    
    res = []

    for e in entreprises:
        distance = int(distance)
        Edist = dist.get(e.id, -1)
        if (Edist != -1):
            if (distance == 0):
                e.dist = Edist
                res.append(e)
            else:
                if (Edist <= distance):
                    e.dist = Edist
                    res.append(e)
    
    print(res)

    def getDist(e):
        return e.dist
    
    res.sort(key=getDist)
    
    rep = {"total" : len(res), "nbPages" : (len(res)//10)+1, "value" : res[(numPage-1)*10:numPage*10]}
    return rep


def delete_token(given_token):
    t = Token.query.filter_by(token=str(given_token)).delete()


def search_entreprises(name):
    return Entreprise.query.filter(Entreprise.nom.like("%"+name+"%"))

def get_entreprise(id):
    return Entreprise.query.filter(Entreprise.id == id)


def get_user(email):
    return User.query.filter_by(email=email).first()


def blacklist_entreprise(id):
    e = Entreprise.query.filter_by(id=id).first()
    if getattr(e, "blacklist") == False:
        setattr(e, "blacklist", True)
    else:
        setattr(e, "blacklist", False)
    db.session.commit()


def get_user(id):
    return User.query.filter_by(id=id).first()


def get_user_id(email):
    u = User.query.filter_by(email=email).first()
    if u != None :
        return u.id
    return None


def get_favoris(id):
    u = User.query.filter_by(id=id).first()
    if u != None :
        return u.favoris


def add_favoris(id, id_entreprise):
    u = User.query.filter_by(id=id).first()
    if u != None :
        e = Entreprise.query.filter_by(id=id_entreprise).first()
        if e!= None:
            u.favoris.append(e)
            db.session.add(u)
            db.session.commit()


def del_favoris(id, id_entreprise):
    u = User.query.filter_by(id=id).first()
    if u != None :
        e = Entreprise.query.filter_by(id=id_entreprise).first()
        u.favoris.remove(e)
        db.session.add(u)
        db.session.commit()


def get_preferences():
    return Preference.query.all()
    

def get_user_prefs(id):
    res = []
    for x in User.query.filter_by(id=id).first().preferences :
        res.append(x.id)
    return res

def get_articles():
    return Article.query.all()

def blacklist_entreprise(id):
    Entreprise.query.filter_by(id=id).first().blacklist = not Entreprise.query.filter_by(id=id).first().blacklist
    db.session.commit()

def modify_admin(id):
    User.query.filter_by(id=id).first().admin = not User.query.filter_by(id=id).first().admin
    db.session.commit()
    return

def is_admin(id, connected):
    if(connected):
        return getattr(User.query.filter_by(id=id).first(), "admin")
    return False

def creer_article(titre, html, image):
    last_article = Article.query.all()[-1]
    f = open("./internking/templates/articles/article" + str(last_article.id+1) + ".html", "w")
    f.write(""" 
    {% include 'base.html' %}

    {% block main %}
    <main>
    """)
    f.write(str(html))
    f.write(""" 
    </main>
        {% endblock %}
    """) 
    f.close()
    a = Article(titre=titre, fileName="article" + str(last_article.id+1) + ".html", image=image)
    db.session.add(a)
    db.session.commit()

def remove_article(id):
    try:
        with open("./internking/templates/articles/article" + str(id) + ".html"):
            os.remove("./internking/templates/articles/article" + str(id) + ".html")
    except IOError:
        print("erreur: fichier non existant")

    Article.query.filter_by(id=id).delete()
    db.session.commit()