from calendar import c
from sqlite3 import connect
from flask import render_template, request, make_response, redirect, Response, jsonify
from .utils import *
from .app import app
import sys
from .models import Article, Entreprise
from .utils import get_element


@app.route('/')
def index(): #retourne sur la page d'index ou si connecté sur la page recherche
    if(verifyConnected()):
        resp = make_response(redirect('/search'))
        return resp
    else :
        return render_template('index.html', cssfile="index", articles=get_articles())

@app.route('/signin', methods=['POST', 'GET'])
def signin(): #fonction s'occupant des pages de connexion et gérant tout les cas possibles pour ne pas créer d'incohérences
    #i.e : page d'inscription alors que déja connecté
    if request.method=="POST":
        mail = request.form.get("mail")
        id = get_user_id(mail)
        if id!=None :
            password = request.form.get("password")
            if mail != None and password != None :
                if(verify_login(id, password)):
                    token = create_token(id)
                    resp = make_response(redirect('/'))
                    resp.set_cookie('token', token)
                    resp.set_cookie('id', str(id))
                    return resp
        return render_template('signin.html', connected=verifyConnected(), cssfile="signin", wrongInfo=True)
    elif request.method == "GET":
        return render_template('signin.html', connected=verifyConnected(), cssfile="signin")

@app.route('/signup', methods=['POST','GET'])
def signup():#fonction s'occupant des pages d'inscription et gérant tout les cas possibles pour ne pas créer d'incohérences
    if request.method == "POST": 
        password = request.form.get("password") 
        cpassword = request.form.get("cpassword")
        if(cpassword == password):
            name = request.form.get("name")
            surname = request.form.get("surname")
            mail    = request.form.get("mail")
            nomv    = request.form.get("nomVoie")
            code    = request.form.get("code")
            city    = request.form.get("city")
            prefs   = request.form.get("prefs[]")
            if prefs == None:
                prefs = []
            created = create_user(name, surname, nomv, code, city, mail, password, prefs)
            if(not created):
                return render_template('signup.html', connected=verifyConnected(), cssfile="signup", alreadyExist=True, prefs=get_preferences())
            elif created :
                resp = make_response(redirect('/'))
                return resp
            return render_template('signup.html', connected=verifyConnected(), cssfile="signup", infoErr=True, prefs=get_preferences())
        else :
            return render_template('signup.html', connected=verifyConnected(), cssfile="signup", passErr=True, prefs=get_preferences())
    elif request.method == "GET":
        return render_template('signup.html', connected=verifyConnected(), cssfile="signup", prefs=get_preferences())


@app.route('/article/<int:id>')
def articleID(id):
    return render_template("/articles/article" + str(get_element(Article, id).id) + ".html", is_admin=verifyAdmin(), connected=verifyConnected(), cssfile="article")

@app.route('/search', methods=['POST','GET'])
def liste_entreprises():#retourne le résultat template d'une recherche par rapport aux entrées de l'utilisateur
    if verifyConnected():
        if request.method == "POST":
            name = request.form.get("search-theme")
            place = request.form.get("search-place")
            activitySector = request.form.get("filter-secteur")
            distance = request.form.get("filter-distance")
            size = request.form.get("filter-taille")
            favories = request.form.get("isFavoris")
            choice = {"name" : name, "place" : place, "activitySector" : activitySector, "distance" : distance, "size" : size, "favories" : favories}
            if (name != "" or place != "" or activitySector != None or distance != None or size != None):
                rep = search(name, place, activitySector, distance, size, favories, get_user(request.cookies.get("id")))
                return render_template('liste_entreprises.html', connected=verifyConnected(), is_admin=verifyAdmin(), cssfile="liste_entreprises", entreprises=rep["value"], nb_entreprises=rep["total"], choix = choice, nbPages=rep["nbPages"], numPage=1, userId = request.cookies.get("id"), userFavoris = get_favoris(request.cookies.get("id")))    
        return render_template('liste_entreprises.html', connected=verifyConnected(), is_admin=verifyAdmin(), cssfile="liste_entreprises", entreprises=[], nb_entreprises=0, choix = dict(), nbPages=1, numPage=1, userId = request.cookies.get("id"), userFavoris = get_favoris(request.cookies.get("id")))
    else :
        return redirect('/')

@app.route('/search/<num>', methods=['POST','GET'])
def liste_entreprises_num(num):
    if verifyConnected():
        if request.method == "POST" and num.isdigit():
            num = int(num)
            name = request.form.get("search-theme")
            place = request.form.get("search-place")
            activitySector = request.form.get("filter-secteur")
            distance = request.form.get("filter-distance")
            size = request.form.get("filter-taille")
            favories = request.form.get("isFavoris")
            choice = {"name" : name, "place" : place, "activitySector" : activitySector, "distance" : distance, "size" : size, "favories" : favories}
            if (name != "" or place != "" or activitySector != None or distance != None or size != None):
                rep = search(name, place, activitySector, distance, size, favories, num, get_user(request.cookies.get("id")))
                return render_template('liste_entreprises.html', connected=verifyConnected(), cssfile="liste_entreprises", entreprises=rep["value"], nb_entreprises=rep["total"], choix = choice, nbPages=rep["nbPages"], numPage=num, userId = request.cookies.get("id"), userFavoris = get_favoris(request.cookies.get("id")))
        return render_template('liste_entreprises.html', connected=verifyConnected(), is_admin=verifyAdmin(), cssfile="liste_entreprises", entreprises=[], nb_entreprises=0, choix = dict(), nbPages=1, numPage=1, userId = request.cookies.get("id"), userFavoris = get_favoris(request.cookies.get("id")))
    
@app.route('/demandes')
def demandes():#retourne la page de demande
    return render_template('demandes.html', connected=verifyConnected(), is_admin=verifyAdmin(), cssfile="demandes")

@app.route('/admin')
def admin():
    return render_template("admin.html", cssfile="admin", connected=verifyConnected(), is_admin=verifyAdmin())


@app.route('/compte')
def compte():
    return render_template('compte.html', connected=verifyConnected(), is_admin=verifyAdmin(), cssfile="compte", user=get_user(request.cookies.get("id")), prefs=get_preferences())

@app.route('/get_user_prefs/<int:id>', methods=["GET"])
def user_prefs(id):
    if request.method == "GET":
        return jsonify(get_user_prefs(id))

@app.route('/compte/edit', methods=["POST"])
def edit_acc():
    if request.method == "POST":
        modify_user(request.cookies.get("id"), request.form.to_dict(flat=False))
    return redirect('/compte')

@app.route('/disconnect')
def disconnect():#s'occupe de déconnecter l'utilisateur
    delete_token(request.cookies.get('token'))
    resp = make_response(redirect('/'))
    resp.set_cookie('token', '')
    resp.set_cookie('id',  '')
    return resp

def verifyConnected():#vérifie si l'utilisateur est connecté
    if verify_token(str(request.cookies.get('token'))):
        return True
    return False

@app.route('/gerer/entreprises', methods=['GET', 'POST'])
def blacklist():
    if(verifyAdmin) :
        liste_entreprises = Entreprise.query.filter(Entreprise.blacklist==False).order_by(Entreprise.nom).all()
        blacklist = Entreprise.query.filter(Entreprise.blacklist==True).order_by(Entreprise.nom).all()
        return render_template('blacklist.html', connected=verifyConnected(), is_admin=verifyAdmin(), cssfile="blacklist", liste_entreprises=liste_entreprises, blacklist=blacklist)
    else :
        return redirect('/')

@app.route('/gerer/entreprises/<int:id>', methods=['POST'])
def blacklister(id):
    if verifyAdmin():
        if request.method == "POST":
            blacklist_entreprise(id)
            return redirect('/gerer/entreprises')

@app.route("/ajouter/admin", methods=['GET', 'POST'])
def add_admin():
    if verifyAdmin():
        liste_users = User.query.filter_by(admin=False).all()
        admins = User.query.filter(User.admin==True)
        return render_template('add_admin.html', user_id=int(request.cookies.get('id')), cssfile="blacklist", connected=verifyConnected(), liste_users=liste_users, admins=admins)
    else :
        return redirect("/")

@app.route('/ajouter/admin/<int:id>', methods=['POST'])
def changer_admin(id):
    if verifyAdmin():
        if request.method == "POST":
            modify_admin(id)
            return redirect('/ajouter/admin')
    else:
        return redirect("/")


@app.route('/rediger/article', methods=["GET", "POST"])
def rediger_article():
    if verifyAdmin():
        if request.method == "GET":
            return render_template('add_article.html', cssfile="ajout_article", connected=verifyConnected(), is_admin=verifyAdmin())
    else :
        return redirect("/")

@app.route("/ajouter/article", methods=["GET"])
def ajouter_article():
    if verifyAdmin():
        if request.method == "GET" :
            titre = request.args.get("titre")
            html = request.args.get("html")
            image = request.args.get("image")
            creer_article(titre, html, image)
            return Response(status=200)
    return redirect("/")

@app.route("/gerer/articles")
def gerer_articles():
    if verifyAdmin():
        articles = Article.query.all()
        return render_template('gerer_articles.html', articles=articles, cssfile="blacklist", connected=verifyConnected(), is_admin=verifyAdmin())
    else :
        return redirect("/")


@app.route("/supprimer/article/<int:id>", methods=["POST"])
def supprimer_article(id):
    if verifyAdmin():
        if request.method == "POST" :
            remove_article(id)
        return redirect("/gerer/articles")
    return redirect("/")

def verifyAdmin():
    return is_admin(request.cookies.get("id"), verifyConnected())


@app.route("/ajouter/favoris/<id>/<idEnt>", methods=["GET"])
def ajoutFavoris(id, idEnt):
    if (verifyConnected()):
        add_favoris(id, idEnt)
        return redirect("/")

@app.route("/supprimer/favoris/<id>/<idEnt>", methods=["GET"])
def supprimmerFavoris(id, idEnt):
    if (verifyConnected()):
        del_favoris(id, idEnt)
        return redirect("/")