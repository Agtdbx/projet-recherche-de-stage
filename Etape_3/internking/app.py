import sqlite3
from flask import Flask
app = Flask(__name__)

app.config['BOOTSTRAP_SERVE_LOCAL'] = True
from flask_bootstrap import Bootstrap
Bootstrap(app)

import os.path


def mkpath(p):
    """
    fonction indicant le path pour l'application
    :param p:
    :return:
    """
    return os.path.normpath(
        os.path.join(
            os.path.dirname(__file__),
            p
        )
    )


from flask_sqlalchemy import SQLAlchemy #les lignes [25..31] servent a lier la base de donnée SQLAlchemy a l'application

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///../database.db'

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)