from .app import db

UsersFavs = db.Table('users_favoris', db.Model.metadata,
                     db.Column('user_id', db.ForeignKey('user.id'), primary_key=True),
                     db.Column('entreprise_id', db.ForeignKey('entreprise.id'), primary_key=True)
                     )

UsersPrefs = db.Table('users_preferences', db.Model.metadata,
                      db.Column('user_id', db.ForeignKey('user.id'), primary_key=True),
                      db.Column('preference_id', db.ForeignKey('preference.id'), primary_key=True)
                      )

SectEntr = db.Table('secteurs_entreprise', db.Model.metadata,
                      db.Column('entreprise_id', db.ForeignKey('entreprise.id'), primary_key=True),
                      db.Column('secteur_id', db.ForeignKey('secteur.id'), primary_key=True)
                      )


class User(db.Model):
    __tablename__ = 'user'  

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(100), unique=True)
    prenom = db.Column(db.String(100))
    nom = db.Column(db.String(100))
    fonction = db.Column(db.String(100))
    admin = db.Column(db.Boolean)

    salt = db.Column(db.String(32))
    key = db.Column(db.String(32))

    favoris = db.relationship("Entreprise", secondary=UsersFavs)
    preferences = db.relationship("Preference", secondary=UsersPrefs)

    adresse_id = db.Column(db.ForeignKey('adresse.id'))
    adresse = db.relationship("Adresse", back_populates="user", uselist=False)

    articles = db.relationship('Article')


class Entreprise(db.Model):
    """la class entreprise représente les entreprises existantes ouverte a une demande de stage"""
    __tablename__ = 'entreprise'

    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(100))
    blacklist = db.Column(db.Boolean)
    taille = db.Column(db.String(10))
    tel = db.Column(db.String(15))
    ape = db.Column(db.String(15))
    activite = db.Column(db.String(150))

    secteur = db.relationship("Secteur", secondary=SectEntr)

    adresse_id = db.Column(db.ForeignKey('adresse.id'))
    adresse = db.relationship("Adresse", back_populates="entreprise", uselist=False)

class Secteur(db.Model):
    __tablename__ = 'secteur'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nom = db.Column(db.String(100))
    ape = db.Column(db.String(15))

class Adresse(db.Model):
    """la class adresse représente la position des deux précedentes classes, celle-ci est mise a part
    car possiblement versatile et pour alleger la BD"""
    __tablename__ = 'adresse'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    voie = db.Column(db.String(200))
    code = db.Column(db.Integer)
    ville = db.Column(db.String(100))

    user = db.relationship("User")
    entreprise = db.relationship("Entreprise")


class Token(db.Model):
    """"la class token permet de creer un code d'accés valable pour 1h pour un utilisateur"""
    user = db.relationship('User', backref=db.backref("Token", lazy="dynamic"))
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), primary_key=True)
    date = db.Column(db.Date)
    token = db.Column(db.String(32))


class Preference(db.Model):
    """la classe preference est utilisé pour le profil utilisateur et la recherche d'entreprise"""
    __tablename__ = 'preference'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))


class Article(db.Model):
    """la classe Article contient les articles divers postés par les administrateurs concernant la recherche de stage
    et les MaJ du site"""
    __tablename__ = 'article'

    id = db.Column(db.Integer, primary_key=True)
    titre = db.Column(db.String(50))
    fileName = db.Column(db.String(50))
    image = db.Column(db.Text)
    auteur_id = db.Column(db.Integer, db.ForeignKey('user.id'))
