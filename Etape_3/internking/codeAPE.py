from bs4 import BeautifulSoup
from urllib.request import urlopen


def wipeHtmlTag(string):
    rep = ""
    goodValue = False
    for i in range(len(string)):
        if string[i] == "<":
            goodValue = False
        elif string[i] == ">":
            goodValue = True
        elif goodValue:
            rep += string[i]

    return rep.strip()



def recupNameFromTr(tr):
    th = tr.find_all("td")

    if len(th) > 0:
        
        return wipeHtmlTag(str(th[0]))

    return None


def recupValueFromTr(tr):
    td = tr.find_all("td")
    if len(td) > 0:
        return wipeHtmlTag(str(td[1]))

    return None


def recupInfo():
    url = "https://www.compte-pro.com/code-naf"
    rep = dict()

    page = urlopen(url)
    html = page.read().decode("utf-8")
    soup = BeautifulSoup(html, "html.parser")

    table = soup.find_all("table")
    tboby = table[0].find_all("tbody")
    tr = tboby[0].find_all("tr")
    
    print(len(tr))
    for i in range(len(tr)):
        k = recupNameFromTr(tr[i])
        key = ""
        for c in k:
            if (c != '.'):
                key += c

        if key != None:
            rep[key] = recupValueFromTr(tr[i])
    return rep

# for key in info.keys():
#     print(key, ":", info[key])